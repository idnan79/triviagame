export const TriviaAPI = {
  fetchQuestions(settings) {
    return fetch(
      `https://opentdb.com/api.php?amount=${settings.number}&category=${settings.category}&difficulty=${settings.difficulty}&type=${settings.type}`
    )
      .then((response) => response.json())
      .then((data) => data.results);
  },
  fetchCategories() {
    return fetch("https://opentdb.com/api_category.php")
      .then((response) => response.json())
      .then((data) => data.trivia_categories);
  },
};
