import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
  {
    path: "/Start",
    alias: "/",
    component: () =>
      import(/* webpackChunkName: "Start" */ "./components/Start/StartPage"),
  },
  {
    path: "/Quiz",
    component: () =>
      import(
        /* webpackChunkName: "Quiz" */ "./components/QuestionGame/MultiQuiz"
      ),
  },
  {
    path: "/Score",
    component: () =>
      import(/* webpackChunkName: "Score" */ "./components/QuestionGame/Score"),
  },
];

const router = new VueRouter({
  routes,
  mode: "history",
});
export default router;
