import Vue from "vue";
import Vuex from "vuex";
import { TriviaAPI } from "@/api/TriviaAPI";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    settings: {
      difficulty: 0,
      number: 0,
      category: 0,
      type: 0,
      step: 0,
    },
    score: 0,
    userAnswers: [],
    categories: [],
    questions: [],
    step: 0,
    fetchingCategores: false,
    fetchingError: "",
    boolscore: true,
  },
  mutations: {
    setFetching: (state, payload) => {
      state.fetchingCategores = payload;
    },
    setFetchingError: (state, payload) => {
      state.fetchingError = payload;
    },
    setQuestions: (state, payload) => {
      state.questions = payload;
    },
    setSelectSetting: (state, payload) => {
      state.settings = payload;
    },
    setCategories: (state, payload) => {
      state.categories = payload;
    },
    setStep: (state, payload) => {
      state.step = payload;
    },
    /* setScore: (state, payload) => {
      state.score = payload;
    }, */
    setUserAnswer: (state, payload) => {
      state.userAnswers.push(payload);
    },
  },
  actions: {
    //here we fetcg
    async fetchCategories({ commit }) {
      try {
        commit("setFetching", true);
        const categories = await TriviaAPI.fetchCategories();
        commit("setCategories", categories);
      } catch (error) {
        commit("setFetchingError", error);
      } finally {
        commit("setFetching", false);
      }
    },
    async fetchQuestions({ commit, state }) {
      const { settings } = state;
      try {
        commit("setFetching", true);
        const questions = await TriviaAPI.fetchQuestions(settings);
        commit("setQuestions", questions);

        // console.log(questions);
      } catch (error) {
        commit("setFetchingError", error);
      } finally {
        commit("setFetching", false);
      }
    },
    async fetchUserAnswer({ commit }) {
      try {
        commit("setFetching", true);
        const userAnswers = await TriviaAPI.fetchUserAnswer();
        commit("setUserAnswer", userAnswers);
      } catch (error) {
        commit("setFetchingError", error);
      } finally {
        commit("setFetching", false);
      }
    },
  },
  getters: {
    numberOfQuestions: (state) => {
      return state.questions.length;
    },
    getCurrentQuestion: (state) => {
      return state.questions[state.step];
    },
    getResult: (state) => {
      let score = 0;
      let maxScore = 0;
      state.questions.forEach((q) => {
        if (q.correct_answer === q.answer) {
          score += 10;
        }
        maxScore += 10;
      });
      return { score, maxScore };
    },
  },
});
